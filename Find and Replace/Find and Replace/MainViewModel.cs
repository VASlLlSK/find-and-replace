﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace Find_and_Replace
{
    public class NPC : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }

    class MainViewModel : NPC
    {
        public FileGetter fileGetter { get; set; }
        public Finder finder { get; set; }
        public Replacer replacer { get; set; }

        string _result;
        public string Result
        {
            get { return _result; }
            set
            {
                _result = value;
                OnPropertyChanged();
            }
        }

        public Stage stage { get; set; }
        public Task _task;

        public MainViewModel()
        {
            fileGetter = new FileGetter();
            finder = new Finder();
            replacer = new Replacer();
            stage = Stage.getInstance();
        }


        private RelayCommand showFolderBrowserDialog;
        public RelayCommand ShowFolderBrowserDialog
        {
            get
            {
                return showFolderBrowserDialog ??
                    (showFolderBrowserDialog = new RelayCommand(x =>
                    {
                        var dir = x as FileGetter;
                        FolderBrowserDialog fbd = new FolderBrowserDialog();
                        fbd.RootFolder = Environment.SpecialFolder.Desktop;
                        fbd.Description = "Select Folder";
                        fbd.ShowNewFolderButton = false;
                        if (fbd.ShowDialog() == DialogResult.OK)
                            dir.DirPath = fbd.SelectedPath;
                    }));
            }
        }

        private RelayCommand findFile;
        public RelayCommand FindFile
        {
            get
            {
                return findFile ??
                    (findFile = new RelayCommand(
                    x => {
                        Task.Factory.StartNew(()=>
                        {
                            Stage s = Stage.getInstance();
                            s.Status = Status.Processing;

                            Result = "";
                            var Fg = x as FileGetter;
                            var filePaths = Fg.GetFiles();

                            if (stage.Status != Status.Cancelled)
                                if (finder.Find.Trim() == "" || finder.Find == null)
                                {
                                    for (int i = 0; i < filePaths.Count; i++)
                                    {
                                        Result += filePaths[i].ToString();
                                        Result += "\n";
                                    }
                                }
                                else
                                {
                                    var result = finder.GetFinded(filePaths);
                                    for (int i = 0; i < result.Count; i++)
                                    {
                                        Result += result[i].FilePath.ToString();
                                        Result += "\n";
                                    }
                                }
                        });
                    }, 
                    x => {
                        var Fg = x as FileGetter;
                        if (Fg != null)
                        {
                            if (Fg.DirPath.Trim() == "" || Fg.FileMasks.Trim() == "")
                                return false;
                            return true;
                        }
                        return false;
                    }));
            }
        }

        private RelayCommand findAndReplaceFile;
        public RelayCommand FindAndReplaceFile
        {
            get
            {
                return findAndReplaceFile ??
                    (findAndReplaceFile = new RelayCommand(
                    x => {
                        Task.Factory.StartNew(() =>
                        {
                            Stage s = Stage.getInstance();
                            s.Status = Status.Processing;

                            var Fg = x as FileGetter;
                            var filePaths = Fg.GetFiles();

                            var result = finder.GetFinded(filePaths);

                            replacer.Replacing(result, finder.Find);

                            if (stage.Status != Status.Cancelled)
                                for (int i = 0; i < result.Count; i++)
                                {
                                Result += result[i].FilePath.ToString();
                                Result += " - replaced\n";
                                }
                        });
                    },
                    x => {
                        var Fg = x as FileGetter;
                        if (Fg != null && replacer !=null)
                        {
                            if (Fg.DirPath.Trim() == "" || Fg.FileMasks.Trim() == "" || finder.Find == "" || replacer.Replace == "")
                                return false;
                            return true;
                        }
                        return false;
                    }));
            }
        }

        private RelayCommand cancel;
        public RelayCommand Cancel
        {
            get
            {
                return cancel ??
                    (cancel = new RelayCommand(x =>
                    {
                        var c = x as Stage;
                        
                        Stage s = Stage.getInstance();
                        s.Status = Status.Cancelled;
                    }));
            }
        }

    }
    public enum Status
    {
        Processing,
        Completed,
        Cancelled
    }

    public class Stage : NPC
    {
        Status status;
        string stageString;
        double progress;
        public Status Status
        {
            get { return status; }
            set
            {
                status = value;
                OnPropertyChanged();
            }
        }
        public string StageString
        {
            get { return stageString; }
            set
            {
                stageString = value;
                OnPropertyChanged();
            }
        }

        public double Progress
        {
            get { return progress; }
            set
            {
                progress = value;
                OnPropertyChanged();
            }
        }

        private static Stage instance;

        private Stage()
        {
            Status = Status.Completed;
            Progress = 0;
            StageString = "";
        }

        public static Stage getInstance()
        {
            if (instance == null)
                instance = new Stage();
            return instance;
        }

    }

    public class RelayCommand : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public RelayCommand(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
    }

    public class FileGetter : NPC, IDataErrorInfo
    {
        string dirPath;
        string fileMasks;
        string excludeFileMasks;

        public string DirPath 
        { 
            get { return dirPath; } 
            set
            {
                dirPath = value;
                OnPropertyChanged();
            } 
        }

        public string FileMasks
        {
            get { return fileMasks; }
            set
            {
                fileMasks = value;
                OnPropertyChanged();
            }
        }

        public string ExcludeFileMasks
        {
            get { return excludeFileMasks; }
            set
            {
                excludeFileMasks = value;
                OnPropertyChanged();
            }
        }

        public FileGetter()
        {
            DirPath = "";
            FileMasks = "";
            ExcludeFileMasks = "";
        }

        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "DirPath":
                        dirPath = dirPath.Trim();
                        if (dirPath == "")
                        {
                            error = "Need to fill out this field";
                        }
                        break;
                    case "FileMasks":
                        fileMasks = fileMasks.Trim();
                        if (fileMasks == "")
                        {
                            error = "Need to fill out this field";
                        }
                        break;
                }
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        private int _fileCount;
        public int FileCount
        {
            get { return _fileCount; }
        }
        public List<string> FileCollection = new List<string>();
        public List<string> GetFiles()
        {
            _fileCount = 0;
            FileCollection.Clear();
            Stage stage = Stage.getInstance();
            
            List<string> masks = FileMasks.Split(',').ToList();            
            List<string> excludeMasks = ExcludeFileMasks.Split(',').ToList();

            foreach (var fileMask in masks)
            {
                if (stage.Status == Status.Cancelled)
                    break;

                stage.StageString = "Search by mask: " + " " + fileMask.Trim();
                var files = Directory.GetFiles(DirPath, fileMask.Trim(), SearchOption.AllDirectories);
                double i = 1;
                foreach (string filePath in files)
                {
                    if (stage.Status == Status.Cancelled)
                        break;

                    stage.Progress = i / files.Length * 100;
                    i++;
                    Thread.Sleep(500);

                    if (!isExcludeFileMasks(filePath, excludeMasks))
                    {
                        _fileCount++;
                        FileCollection.Add(filePath);
                    }

                }
            }
            return FileCollection;
        }

        public bool isExcludeFileMasks(string path, List<string> excludeMasks)
        {
            if (ExcludeFileMasks == null)
                return false;

            var ExcludeFileMasksRegEx = excludeMasks.Select(WildcardToRegex).ToList();
            
            foreach (string pattern in ExcludeFileMasksRegEx)
            {
                if (Regex.IsMatch(path, pattern.Trim()))
                    return true;
            }

            return false;
        }

        static string WildcardToRegex(string pattern)
        {
            return string.Format("^{0}$", Regex.Escape(pattern).Replace("\\*", ".*").Replace("\\?", "."));
        }

    }

    public class Finder : NPC
    {
        string find;
        public string Find
        {
            get { return find; }
            set
            {
                find = value;
                OnPropertyChanged();
            }
        }

        List<string> paths;

        public Finder()
        {
            Find = "";
        }
        public List<ResultItem> GetFinded(List<string> Paths)
        {
            paths = Paths;
            List<ResultItem> resultItems = new List<ResultItem>();
            Stage stage = Stage.getInstance();
            stage.StageString = "Search for entries";
            double i = 1;
            foreach (string filePath in paths)
            {
                if (stage.Status == Status.Cancelled)
                    break;

                stage.Progress = i / paths.Count * 100;
                i++;
                Thread.Sleep(500);

                var item = FindInFile(filePath);
                if (item.finded)
                {
                    resultItems.Add(item);
                }

            }
            return resultItems;
        }

        ResultItem FindInFile(string path)
        {
            var resultItem = new ResultItem();
            resultItem.FilePath = path;
            string fileContent;
            using (var sr = new StreamReader(path))
            {
                fileContent = sr.ReadToEnd();
            }

            resultItem.Matches = FindMatches(fileContent, find);

            if(resultItem.Matches == 0)
                resultItem.finded = false;
            else
            {
                resultItem.finded = true;
                resultItem.FileContent = fileContent;
            }

            return resultItem;
        }

        int FindMatches(string fileContent, string findText)
        {
            MatchCollection matches;
            matches = Regex.Matches(fileContent, findText, RegexOptions.Multiline);

            return matches.Count;
        }
    }

    public class ResultItem
    {
        public string FilePath { get; set; }
        public int Matches { get; set; }
        public string FileContent { get; set; }
        public bool finded { get; set; }
    }

    public class Replacer : NPC
    {
        string replace;
        public string Replace
        {
            get { return replace; }
            set
            {
                replace = value;
                OnPropertyChanged();
            }
        }
        List<ResultItem> resultItem;
        string FindText;
        public Replacer()
        {
            Replace = "";
        }
        public void Replacing(List<ResultItem> ResultItem, string Find)
        {
            resultItem = ResultItem;
            FindText = Find;

            Stage stage = Stage.getInstance();
            stage.StageString = "Replacement of entries";
            double i = 1;
            foreach (var item in resultItem)
            {
                if (stage.Status == Status.Cancelled)
                    break;

                stage.Progress = i / resultItem.Count * 100;
                i++;
                Thread.Sleep(500);

                item.FileContent = ReplaceInContent(item);

                using (var sw = new StreamWriter(item.FilePath, false))
                {
                    sw.Write(item.FileContent);
                }

            }
        }

        string ReplaceInContent(ResultItem item)
        {
            return Regex.Replace(item.FileContent, FindText, Replace);
        }

    }

}